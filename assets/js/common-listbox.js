﻿
function init_compose() {
    $("#compose, .compose-close").click(function () {
        $(".compose").slideToggle()
    })
}
init_compose();
$(document).ready(function () {
    $(".collapse-link").on("click", function () {
        var a = $(this).closest(".x_panel"),
            b = $(this).find("i"),
            c = a.find(".x_content");
        a.attr("style") ? c.slideToggle(200, function () {
            a.removeAttr("style")
        }) : (c.slideToggle(200), a.css("height", "auto")), b.toggleClass("fa-chevron-up fa-chevron-down")
    }), $(".close-link").click(function () {
        var a = $(this).closest(".x_panel");
        a.remove()
    })
}), $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
        container: "body"
    })
}), $(document).ready(function () {
    $("input.flat")[0] && $(document).ready(function () {
        $("input.flat").iCheck({
            checkboxClass: "icheckbox_flat-green",
            radioClass: "iradio_flat-green"
        })
    })
}), $("table input").on("ifChecked", function () {
    checkState = "", $(this).parent().parent().parent().addClass("selected"), countChecked()
}), $("table input").on("ifUnchecked", function () {
    checkState = "", $(this).parent().parent().parent().removeClass("selected"), countChecked()
});
var checkState = "";
$(".bulk_action input").on("ifChecked", function () {
    checkState = "", $(this).parent().parent().parent().addClass("selected"), countChecked()
}), $(".bulk_action input").on("ifUnchecked", function () {
    checkState = "", $(this).parent().parent().parent().removeClass("selected"), countChecked()
}), $(".bulk_action input#check-all").on("ifChecked", function () {
    checkState = "all", countChecked()
}), $(".bulk_action input#check-all").on("ifUnchecked", function () {
    checkState = "none", countChecked()
}), $(document).ready(function () {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200), $expand = $(this).find(">:first-child"), "+" == $expand.text() ? $expand.text("-") : $expand.text("+")
    })
}), "undefined" != typeof NProgress && ($(document).ready(function () {
    NProgress.start()
}), $(window).load(function () {
    NProgress.done()
}));
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function (a) {
    var c, d, b = a instanceof this.constructor ? a : $(a.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
    originalLeave.call(this, a), a.currentTarget && (c = $(a.currentTarget).siblings(".popover"), d = b.timeout, c.one("mouseenter", function () {
        clearTimeout(d), c.one("mouseleave", function () {
            $.fn.popover.Constructor.prototype.leave.call(b, b)
        })
    }))
}, $("body").popover({
    selector: "[data-popover]",
    trigger: "click hover",
    delay: {
        show: 50,
        hide: 400
    }
}), $(document).ready(function () {
});
function initSearchText() {
    var item = document.getElementsByClassName("item-scroll-listbox");
    var searchtext = document.getElementById('search-listbox').value;
    for (var i = 0; i < item.length; i++) {
        if (!item[i].getElementsByTagName('h3')[0].innerText.toUpperCase().includes(searchtext.toUpperCase())) {
            item[i].classList.add('hide');
        } else {
            item[i].classList.remove('hide');
        }
    }
}

function initSearchTextKH() {
    var item = document.getElementsByClassName("item-scroll-listbox");
    var searchtext = document.getElementById('search-listboxKH').value;
    for (var i = 0; i < item.length; i++) {
        if (!item[i].getElementsByTagName('h3')[0].innerText.toUpperCase().includes(searchtext.toUpperCase())) {
            item[i].classList.add('hide');
        } else {
            item[i].classList.remove('hide');
        }
    }
}

function initSearchTextCB() {
    var item = document.getElementsByClassName("item-scroll-listbox");
    var searchtext = document.getElementById('search-listboxCB').value;
    for (var i = 0; i < item.length; i++) {
        if (!item[i].getElementsByTagName('h3')[0].innerText.toUpperCase().includes(searchtext.toUpperCase())) {
            item[i].classList.add('hide');
        } else {
            item[i].classList.remove('hide');
        }
    }
}
function initSearchTextKHcomment() {
    var item = document.getElementsByClassName("media event");
    var searchtext = document.getElementById('search-listboxKHcomment').value;
    for (var i = 0; i < item.length; i++) {
        if (!item[i].getElementsByTagName('p')[0].innerText.toUpperCase().includes(searchtext.toUpperCase())) {
            item[i].classList.add('hide');
        } else {
            item[i].classList.remove('hide');
        }
    }
}

function initSearchTextLich() {
    var item = document.getElementsByClassName("media event");
    var searchtext = document.getElementById('search-listboxLich').value;
    for (var i = 0; i < item.length; i++) {
        if (!item[i].getElementsByTagName('p')[0].innerText.toUpperCase().includes(searchtext.toUpperCase())) {
            item[i].classList.add('hide');
        } else {
            item[i].classList.remove('hide');
        }
    }
}
function initFlip() {
    $(document).on("click", ".item-scroll-listbox", function () {
        $(document.getElementsByClassName('flip-custom')).toggleClass('hover');
    });
}
initFlip();

function reload(d1) {
    //$(document.getElementsByClassName('flip-custom')).toggleClass('hover');
}

//Begin tranform
var ActiveDivTranform = 'tranform-lich';
function ActiveDiv(d1) {
    var item = document.getElementsByClassName("tranform-tacnghiep");
    //if (d1.getAttribute('idname') == ActiveDivTranform) {
    //    return;
    //}
    for (var i = 0; i < item.length; i++) {
        item[i].classList.remove('hide');
    }
    for (var i = 0; i < item.length; i++) {
        if (d1.getAttribute('idname') != item[i].id) {
            item[i].classList.add('hide');
        }
        item[i].setAttribute('style', 'transform: rotateY(-180deg); backface-visibility:hidden;');
        item[i].classList.remove('hover');
    }
    for (var i = 0; i < item.length; i++) {
        if (d1.getAttribute('idname') == item[i].id) {
            item[i].setAttribute('style', 'transform:rotateY(-180deg);backface-visibility:unset;');

            item[i].classList.remove('hide');
            $(item[i]).toggleClass('hover');
            ActiveDivTranform = item[i].id;
            break;
        }
    }
    setTimeout(function () {
        for (var i = 0; i < item.length; i++) {
            if (d1.getAttribute('idname') != item[i].id) {
                item[i].setAttribute('style', 'transform:rotateY(-180deg);backface-visibility:hidden;');
                item[i].classList.remove('hide');
            }
        }
    }, 100);
    for (var i = 0; i < item.length; i++) {
        if (d1.getAttribute('idname') == item[i].id) {
            var tem = document.getElementById('card-panel');
            tem.style.height = (item[i].clientHeight + 80) + 'px';
            break;
        }
    }
}
function inittranform() {
    var tem = $("[idname='" + ActiveDivTranform + "']")[0];
    ActiveDiv(tem);
    var item = document.getElementsByClassName("tranform-tacnghiep");
    for (var i = 0; i < item.length; i++) {
        if (ActiveDivTranform == item[i].id) {
            var tem = document.getElementById('card-panel');
            tem.style.height = (item[i].clientHeight + 80) + 'px';
            break;
        }
    }
}
setTimeout(inittranform, 2000);
//End tranform